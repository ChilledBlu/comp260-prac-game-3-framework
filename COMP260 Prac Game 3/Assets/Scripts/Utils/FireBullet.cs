﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {

	public BulletMove bulletPrefab;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.timeScale == 0) {
			return;
		}

		if (Input.GetButtonDown ("Fire1")) {
			BulletMove bullet = Instantiate (bulletPrefab);

			bullet.transform.position = transform.position;

			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			bullet.direction = ray.direction;
		}
	}
}
